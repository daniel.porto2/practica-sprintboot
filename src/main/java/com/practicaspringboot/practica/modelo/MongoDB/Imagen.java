package com.practicaspringboot.practica.modelo.MongoDB;

import com.practicaspringboot.practica.modelo.MySQL.Persona;
import lombok.*;
import org.bson.types.Binary;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Document(collection = "imagenes")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Imagen {
    @Id
    private String id;

    @OneToOne
    @JoinColumn(name = "personaId", referencedColumnName = "id", columnDefinition = "Int64")
    @NotNull
    @Indexed(unique = true)
    private long personaId;

    private byte[] file;
}
