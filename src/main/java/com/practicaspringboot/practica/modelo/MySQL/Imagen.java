package com.practicaspringboot.practica.modelo.MySQL;

import javax.persistence.*;

import com.sun.istack.NotNull;
import lombok.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(
        name="imagenes",
        schema = "imagenesSchema"
)
public class Imagen {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    @JoinColumn(name = "persona_id", nullable = false, unique = true)
    @NotNull
    private Persona persona;

    @Lob
    @Basic
    @Column(name="file", columnDefinition = "longblob", nullable = true)
    private byte[] file;
}
