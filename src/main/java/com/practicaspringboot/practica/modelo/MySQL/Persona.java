package com.practicaspringboot.practica.modelo.MySQL;

import javax.persistence.*;

import lombok.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(
        name="personas",
        schema = "personasSchema",
        indexes = {@Index(name = "numeroDocumento_index", columnList = "numero_documento", unique = true)}
)
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String tipoDocumento;

    @Column(name = "numero_documento", nullable = false, length = 20)
    private String numeroDocumento;

    private String nombres;

    private String apellidos;

    private int edad;

    private String paisOrigen;

    private String sexo;
}
