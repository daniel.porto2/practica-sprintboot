package com.practicaspringboot.practica.modelo.dto.imagen;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ImagenResponseDTO {
    private String id;

    private long personaId;

    private byte[] file;
}
