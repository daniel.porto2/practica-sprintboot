package com.practicaspringboot.practica.modelo.dto.persona;

public class PersonaResponseDto {
    private String tipoDocumento;

    private String numeroDocumento;

    private String nombres;

    private String apellidos;

    private Integer edad;

    private String paisOrigen;

    private String sexo;
}
