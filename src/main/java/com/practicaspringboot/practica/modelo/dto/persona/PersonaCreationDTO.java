package com.practicaspringboot.practica.modelo.dto.persona;

import lombok.*;

@Getter
@Setter
@ToString(includeFieldNames=true)
@AllArgsConstructor
public class PersonaCreationDTO {

    @NonNull
    private String tipoDocumento;

    @NonNull
    private String numeroDocumento;

    @NonNull
    private String nombres;

    @NonNull
    private String apellidos;

    @NonNull
    private Integer edad;

    private String paisOrigen;

    private String sexo;
}
