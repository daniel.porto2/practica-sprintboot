package com.practicaspringboot.practica.modelo.dto.persona;

import lombok.*;

@Getter
@Setter
@ToString(includeFieldNames=true)
@AllArgsConstructor
@NoArgsConstructor
public class PersonaUpdateDTO {

    private String tipoDocumento;

    private String numeroDocumento;

    private String nombres;

    private String apellidos;

    private Integer edad;

    private String paisOrigen;

    private String sexo;
}
