package com.practicaspringboot.practica.modelo.dto.imagen;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
@ToString(includeFieldNames=true)
@AllArgsConstructor
public class ImagenCreationDTO {

    @NonNull
    private MultipartFile file;
}
