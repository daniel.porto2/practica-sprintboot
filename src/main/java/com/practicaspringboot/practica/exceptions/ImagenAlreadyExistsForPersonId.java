package com.practicaspringboot.practica.exceptions;

public class ImagenAlreadyExistsForPersonId extends RuntimeException{
    public static final String DESCRIPTION = "Image already exists";

    public ImagenAlreadyExistsForPersonId() {
        super(DESCRIPTION);
    }

    public ImagenAlreadyExistsForPersonId(String detail) {
        super(DESCRIPTION + ". " + detail);
    }
}
