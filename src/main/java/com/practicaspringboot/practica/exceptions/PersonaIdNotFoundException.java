package com.practicaspringboot.practica.exceptions;

public class PersonaIdNotFoundException extends RuntimeException{
    public static final String DESCRIPTION = "Persona Id not found";

    public PersonaIdNotFoundException() {
        super(DESCRIPTION);
    }

    public PersonaIdNotFoundException(String detail) {
        super(DESCRIPTION + ". " + detail);
    }
}
