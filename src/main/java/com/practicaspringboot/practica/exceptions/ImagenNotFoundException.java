package com.practicaspringboot.practica.exceptions;

public class ImagenNotFoundException extends RuntimeException{
    public static final String DESCRIPTION = "Imagen not found";

    public ImagenNotFoundException() {
        super(DESCRIPTION);
    }

    public ImagenNotFoundException(String detail) {
        super(DESCRIPTION + ". " + detail);
    }
}