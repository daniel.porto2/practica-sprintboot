package com.practicaspringboot.practica.exceptions;

public class NumeroDocumentoAlreadyExists extends RuntimeException{
    public static final String DESCRIPTION = "Document number already exists";

    public NumeroDocumentoAlreadyExists() {
        super(DESCRIPTION);
    }

    public NumeroDocumentoAlreadyExists(String detail) {
        super(DESCRIPTION + ". " + detail);
    }
}
