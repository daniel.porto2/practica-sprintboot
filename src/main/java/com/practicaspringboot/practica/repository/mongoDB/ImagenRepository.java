package com.practicaspringboot.practica.repository.mongoDB;

import com.practicaspringboot.practica.modelo.MongoDB.Imagen;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ImagenRepository extends MongoRepository<Imagen, String> {
    Optional<Imagen> findByPersonaId(long id);
    boolean existsByPersonaId(long personaId);
}
