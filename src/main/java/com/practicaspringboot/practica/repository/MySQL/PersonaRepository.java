package com.practicaspringboot.practica.repository.MySQL;

import com.practicaspringboot.practica.modelo.MySQL.Persona;
import org.springframework.data.repository.CrudRepository;

public interface PersonaRepository extends CrudRepository<Persona, Long> {
    Persona findByNumeroDocumento(String numeroDocumento);
    boolean existsByNumeroDocumento(String NumeroDocumento);
}
