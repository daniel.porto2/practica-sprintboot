package com.practicaspringboot.practica.util.DataTests;

import com.practicaspringboot.practica.modelo.MongoDB.Imagen;

import java.nio.charset.StandardCharsets;

public class DatosImagen {
    public static Imagen IMAGEN0 = new Imagen("47783578834", 0L, "e04fd020ea3a6910a2d808002b30309d".getBytes(StandardCharsets.UTF_8));
    public static Imagen IMAGEN1 = new Imagen("d873dudbbd3", 1L, "883fruf88hiu3f8cehwniwj038884484n".getBytes(StandardCharsets.UTF_8));
}
