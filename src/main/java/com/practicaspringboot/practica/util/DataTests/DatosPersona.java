package com.practicaspringboot.practica.util.DataTests;

import com.practicaspringboot.practica.modelo.MySQL.Persona;

import java.util.Arrays;
import java.util.List;

public class DatosPersona {
    public static Persona NEW_PERSONA = new Persona(null, "CE", "12848908968", "Jaime", "Enrique Rodríguez", 22, "Perú", "M");
    public static Persona PERSONA0 = new Persona(0L, "CC", "3792469932", "Daniel", "Porto Rojas", 22, "Colombia", "M");
    public static Persona PERSONA1 = new Persona(1L, "CC", "844633804", "Enrique", "Hernandez Oviedo", 29, "Perú", "M");
    public static List<Persona> PERSONALIST = Arrays.asList(PERSONA0, PERSONA1);
}
