package com.practicaspringboot.practica.converters;

import com.practicaspringboot.practica.modelo.MongoDB.Imagen;
import com.practicaspringboot.practica.modelo.dto.imagen.ImagenCreationDTO;
import com.practicaspringboot.practica.modelo.dto.imagen.ImagenResponseDTO;
import com.practicaspringboot.practica.modelo.dto.imagen.ImagenUpdateDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class ImagenModelMapper {
    @Autowired
    private ModelMapper mapper;

    public Imagen ImagenCreationDtoToImagen(ImagenCreationDTO imagenCreationDTO) {
            return mapper.typeMap(ImagenCreationDTO.class, Imagen.class)
                    .addMapping(imgDto -> {
                        try {
                            return imgDto.getFile().getBytes();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return imgDto;
                    }, Imagen::setFile).map(imagenCreationDTO);
    }

    public ImagenResponseDTO imagenToImagenResponseDto(Imagen imagen) {
        return mapper.typeMap(Imagen.class, ImagenResponseDTO.class).map(imagen);
    }

    public Imagen ImagenUpdateDtoToImagen(ImagenUpdateDTO imagenUpdateDTO) {
        return mapper.typeMap(ImagenUpdateDTO.class, Imagen.class)
                .addMapping(imgDto -> {
                    try {
                        return imgDto.getFile().getBytes();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return imgDto;
                }, Imagen::setFile).map(imagenUpdateDTO);
    }

    public Imagen ImagenToImagen(Imagen imagen1, Imagen imagen2) {
        mapper.typeMap(Imagen.class, Imagen.class).addMappings(mpr -> mpr.skip(Imagen::setId))
                .addMappings(mpr -> mpr.skip(Imagen::setPersonaId)).map(imagen1, imagen2);
        return imagen2;
    }
}
