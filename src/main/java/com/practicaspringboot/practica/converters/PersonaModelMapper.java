package com.practicaspringboot.practica.converters;

import com.practicaspringboot.practica.modelo.MongoDB.Imagen;
import com.practicaspringboot.practica.modelo.MySQL.Persona;
import com.practicaspringboot.practica.modelo.dto.persona.PersonaCreationDTO;
import com.practicaspringboot.practica.modelo.dto.persona.PersonaUpdateDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PersonaModelMapper {
    @Autowired
    private ModelMapper modelMapper;

    public Persona personaCreationDtoToPersona(PersonaCreationDTO personaCreationDTO) {
        return modelMapper.typeMap(PersonaCreationDTO.class, Persona.class).map(personaCreationDTO);
    }

    public Persona personaUpdateDtoToPersona(PersonaUpdateDTO personaUpdateDTO) {
        return modelMapper.typeMap(PersonaUpdateDTO.class, Persona.class).map(personaUpdateDTO);
    }

    public Persona personaToPersona(Persona persona1, Persona persona2) {
        modelMapper.typeMap(Persona.class, Persona.class).
        addMappings(mpr -> mpr.skip(Persona::setId))
        .map(persona1, persona2);
        return persona2;
    }
}
