package com.practicaspringboot.practica.rest;

import com.practicaspringboot.practica.converters.PersonaModelMapper;
import com.practicaspringboot.practica.exceptions.NumeroDocumentoAlreadyExists;
import com.practicaspringboot.practica.exceptions.PersonaIdNotFoundException;
import com.practicaspringboot.practica.modelo.MySQL.Persona;
import com.practicaspringboot.practica.modelo.dto.persona.PersonaCreationDTO;
import com.practicaspringboot.practica.modelo.dto.persona.PersonaUpdateDTO;
import com.practicaspringboot.practica.service.persona.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/personas")
public class PersonaRestController {

    @Autowired
    private PersonaService personaService;

    @Autowired
    private PersonaModelMapper personaModelMapper;

    //-------------------Traer todas las Personas --------------------------------------------------------

    @GetMapping
    public ResponseEntity<List<Persona>> listAllPersonas() {
        List<Persona> personas = personaService.findAllPersonas();
        if(personas.isEmpty()){
            return new ResponseEntity<List<Persona>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<Persona>>(personas, HttpStatus.OK);
    }

    //-------------------Traer una persona--------------------------------------------------------

    @GetMapping("/{id}")
    public ResponseEntity<Persona> getPersona(@PathVariable("id") long id) {
        System.out.println("Fetching Persona with id " + id);

        if(!personaService.existsPersonaById(id)) {
            throw new PersonaIdNotFoundException("Persona with id "+id+" not found");
        }

        Optional<Persona> persona = personaService.findById(id);

        return new ResponseEntity<Persona>(persona.get(), HttpStatus.OK);
    }

    //-------------------Crear una Persona--------------------------------------------------------

    @PostMapping
    public ResponseEntity<Void> createPersona(@Valid @RequestBody PersonaCreationDTO personaCreationDTO, UriComponentsBuilder ucBuilder) {

        if(personaService.existsPersonaByNumeroDocumento(personaCreationDTO.getNumeroDocumento())){
            throw new NumeroDocumentoAlreadyExists("The person with document number " + personaCreationDTO.getNumeroDocumento() + " already exists in our system");
        }

        Persona persona = personaModelMapper.personaCreationDtoToPersona(personaCreationDTO);

        Persona savedPersona = personaService.savePersona(persona);

        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ucBuilder.path("/{id}").buildAndExpand(savedPersona.getId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

//------------------- Actualizar una Persona --------------------------------------------------------

    @PutMapping("/{id}")
    public ResponseEntity<Persona> updatePersona(@PathVariable("id") long id,  @Valid @RequestBody PersonaUpdateDTO personaUpdateDTO) {
        System.out.println("Updating Persona " + id);

        if(!personaService.existsPersonaById(id)) {
            throw new PersonaIdNotFoundException("Persona with id "+id+" not found");
        }

        Optional<Persona> currentPersona = personaService.findById(id);
        Persona personaNewFields = personaModelMapper.personaUpdateDtoToPersona(personaUpdateDTO);
        Persona updatedPersona = personaModelMapper.personaToPersona(personaNewFields, currentPersona.get());

        personaService.updatePersona(updatedPersona);
        return new ResponseEntity<Persona>(updatedPersona, HttpStatus.OK);
    }

    //------------------- Eliminar una Persona --------------------------------------------------------

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePersona(@PathVariable("id") long id) {
        System.out.println("Fetching & Deleting Persona with id " + id);

        if(!personaService.existsPersonaById(id)) {
            throw new PersonaIdNotFoundException("Persona with id "+id+" not found");
        }

        personaService.deletePersonaById(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    //------------------- Eliminar todas las Personas --------------------------------------------------------

    @DeleteMapping
    public ResponseEntity<Void> deleteAllPersonas() {
        System.out.println("Deleting All Personas");

        personaService.deleteAllPersonas();
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
