package com.practicaspringboot.practica.rest;

import com.practicaspringboot.practica.converters.ImagenModelMapper;
import com.practicaspringboot.practica.exceptions.ImagenAlreadyExistsForPersonId;
import com.practicaspringboot.practica.exceptions.ImagenNotFoundException;
import com.practicaspringboot.practica.exceptions.PersonaIdNotFoundException;
import com.practicaspringboot.practica.modelo.MongoDB.Imagen;
import com.practicaspringboot.practica.modelo.dto.imagen.ImagenCreationDTO;
import com.practicaspringboot.practica.modelo.dto.imagen.ImagenResponseDTO;
import com.practicaspringboot.practica.modelo.dto.imagen.ImagenUpdateDTO;
import com.practicaspringboot.practica.service.imagen.ImagenService;
import com.practicaspringboot.practica.service.persona.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/api/personas/{personaId}/imagen")
public class ImagenRestController {

    @Autowired
    private ImagenService imagenService;

    @Autowired
    private PersonaService personaService;

    @Autowired
    private ImagenModelMapper imagenModelMapper;


    //------------------- Crear una Imagen --------------------------------------------------------
    @PostMapping
    public ResponseEntity<ImagenResponseDTO> createImagen(@PathVariable("personaId") long personaId, @RequestPart MultipartFile file, @ModelAttribute ImagenCreationDTO imagenCreationDTO) {

        if(!personaService.existsPersonaById(personaId)){
            throw new PersonaIdNotFoundException("Persona with id " + personaId + " not found");
        }
        if(imagenService.existsImagenByPersonaId(personaId)){
            throw new ImagenAlreadyExistsForPersonId("The person with id " + personaId + " already has an image");
        }
        Imagen imagen = imagenModelMapper.ImagenCreationDtoToImagen(imagenCreationDTO);
        imagen.setPersonaId(personaId);
        imagen = imagenService.saveImagen(imagen);
        ImagenResponseDTO imgDto = imagenModelMapper.imagenToImagenResponseDto(imagen);
        return new ResponseEntity<ImagenResponseDTO>(imgDto, HttpStatus.CREATED);
    }

    //------------------- Eliminar Imagen --------------------------------------------------------
    @DeleteMapping
    public ResponseEntity<Void> deleteImagen(@PathVariable("personaId") long personaId) {
        if(!imagenService.existsImagenByPersonaId(personaId)) {
            throw new ImagenNotFoundException("Imagen of Persona with id " + personaId + " not found");
        }
        Optional<Imagen> imagen = imagenService.findImagenByPersonaId(personaId);
        imagenService.deleteById(imagen.get().getId());
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    //-------------------Traer una Imagen--------------------------------------------------------

    @GetMapping
    public ResponseEntity<ImagenResponseDTO> getImagen(@PathVariable("personaId") long personaId) {
        if(!imagenService.existsImagenByPersonaId(personaId)) {
            throw new ImagenNotFoundException("Imagen of Persona with id " + personaId + " not found");
        }
        Optional<Imagen> imagen = imagenService.findImagenByPersonaId(personaId);
        ImagenResponseDTO imgDto = imagenModelMapper.imagenToImagenResponseDto(imagen.get());
        return new ResponseEntity<ImagenResponseDTO>(imgDto, HttpStatus.OK);
    }

    //-------------------Actualizar una Imagen--------------------------------------------------------
    @PutMapping
    public ResponseEntity<ImagenResponseDTO> updateImagen(@PathVariable("personaId") long personaId, @RequestPart MultipartFile file, @Valid ImagenUpdateDTO imagenUpdateDTO) throws IOException {
        if(!imagenService.existsImagenByPersonaId(personaId)) {
            throw new ImagenNotFoundException("Imagen of Persona with id " + personaId + " not found");
        }

        Optional<Imagen> currentImagen = imagenService.findImagenByPersonaId(personaId);
        Imagen recievedImagen = imagenModelMapper.ImagenUpdateDtoToImagen(imagenUpdateDTO);
        Imagen updatedImagen = imagenModelMapper.ImagenToImagen(recievedImagen, currentImagen.get());
        imagenService.updateImagen(updatedImagen);
        ImagenResponseDTO imgDto = imagenModelMapper.imagenToImagenResponseDto(updatedImagen);
        return new ResponseEntity<ImagenResponseDTO>(imgDto, HttpStatus.OK);
    }

}
