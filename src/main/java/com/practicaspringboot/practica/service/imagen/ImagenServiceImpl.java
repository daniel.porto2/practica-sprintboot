package com.practicaspringboot.practica.service.imagen;

import com.practicaspringboot.practica.modelo.MongoDB.Imagen;
import com.practicaspringboot.practica.repository.mongoDB.ImagenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ImagenServiceImpl implements ImagenService{

    @Autowired
    private ImagenRepository imagenRepository;

    @Override
    public Optional<Imagen> findById(String id) {
        return imagenRepository.findById(id);
    }

    @Override
    public Imagen saveImagen(Imagen imagen) {
        return imagenRepository.save(imagen);
    }

    @Override
    public Imagen updateImagen(Imagen imagen) {
        return imagenRepository.save(imagen);
    }

    @Override
    public void deleteById(String id) {
        imagenRepository.deleteById(id);
    }

    @Override
    public Optional<Imagen> findImagenByPersonaId(long personaId) {
        return imagenRepository.findByPersonaId(personaId);
    }

    @Override
    public boolean existsImagenById(String id){return imagenRepository.existsById(id);}

    @Override
    public boolean existsImagenByPersonaId(long personaId){return imagenRepository.existsByPersonaId(personaId);}
}
