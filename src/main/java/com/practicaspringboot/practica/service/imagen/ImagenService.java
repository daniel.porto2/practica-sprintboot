package com.practicaspringboot.practica.service.imagen;

import com.practicaspringboot.practica.modelo.MongoDB.Imagen;

import java.util.Optional;

public interface ImagenService {
    Optional<Imagen> findById(String id);

    Imagen saveImagen(Imagen imagen);

    Imagen updateImagen(Imagen imagen);

    void deleteById(String id);

    Optional<Imagen> findImagenByPersonaId(long id);

    public boolean existsImagenById(String id);

    public boolean existsImagenByPersonaId(long personaId);
}
