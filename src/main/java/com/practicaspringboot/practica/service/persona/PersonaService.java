package com.practicaspringboot.practica.service.persona;

import com.practicaspringboot.practica.modelo.MySQL.Persona;

import java.util.List;
import java.util.Optional;

public interface PersonaService {
    Optional<Persona> findById(long id);

    Persona findByNumeroDocumento(String name);

    Persona savePersona(Persona persona);

    Persona updatePersona(Persona persona);

    void deletePersonaById(long id);

    List<Persona> findAllPersonas();

    void deleteAllPersonas();

    public boolean existsPersonaById(long id);

    public boolean existsPersonaByNumeroDocumento(String numeroDocumento);
}
