package com.practicaspringboot.practica.service.persona;

import com.practicaspringboot.practica.modelo.MySQL.Persona;
import com.practicaspringboot.practica.repository.MySQL.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PersonaServiceImpl implements PersonaService{

    @Autowired
    private PersonaRepository personaRepository;

    private static List<Persona> personas;

    public List<Persona> findAllPersonas() {
        return (List<Persona>) personaRepository.findAll();
    }

    public Optional<Persona> findById(long id) {
        return personaRepository.findById(id);
    }

    public Persona findByNumeroDocumento(String numeroDocumento) {
        return personaRepository.findByNumeroDocumento(numeroDocumento);
    }

    public Persona savePersona(Persona persona) {
        return personaRepository.save(persona);
    }

    public Persona updatePersona(Persona persona) {
        System.out.println(persona);
        return personaRepository.save(persona);
    }

    public void deletePersonaById(long id) {personaRepository.deleteById(id);}

    public void deleteAllPersonas(){
        personaRepository.deleteAll();
    }

    public boolean existsPersonaById(long id){return personaRepository.existsById(id);}

    public boolean existsPersonaByNumeroDocumento(String numeroDocumento) { return personaRepository.existsByNumeroDocumento(numeroDocumento); }
}
