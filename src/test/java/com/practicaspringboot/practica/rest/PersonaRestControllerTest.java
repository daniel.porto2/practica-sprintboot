package com.practicaspringboot.practica.rest;

import com.practicaspringboot.practica.converters.PersonaModelMapper;
import com.practicaspringboot.practica.exceptions.NumeroDocumentoAlreadyExists;
import com.practicaspringboot.practica.exceptions.PersonaIdNotFoundException;
import com.practicaspringboot.practica.modelo.MySQL.Persona;
import com.practicaspringboot.practica.modelo.dto.persona.PersonaCreationDTO;
import com.practicaspringboot.practica.modelo.dto.persona.PersonaUpdateDTO;
import com.practicaspringboot.practica.service.persona.PersonaService;
import com.practicaspringboot.practica.util.DataTests.DatosPersona;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class PersonaRestControllerTest {

    @Mock
    private PersonaService personaService;

    @Mock
    private PersonaModelMapper personaModelMapper;

    @InjectMocks
    private PersonaRestController personaRestController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void listAllPersonas() {
        when(personaService.findAllPersonas()).thenReturn(DatosPersona.PERSONALIST);
        ResponseEntity<List<Persona>> result = personaRestController.listAllPersonas();
        assertEquals(2, result.getBody().size());
        assertEquals(HttpStatus.OK.value(), result.getStatusCode().value());
    }

    @Test
    void getPersona() {
        when(personaService.findById(1L)).thenReturn(Optional.ofNullable(DatosPersona.PERSONA1));
        when(personaService.existsPersonaById(1L)).thenReturn(true);
        ResponseEntity<Persona> result1 = personaRestController.getPersona(1L);
        assertEquals("844633804", result1.getBody().getNumeroDocumento());
        assertEquals(HttpStatus.OK.value(), result1.getStatusCode().value());

        when(personaService.existsPersonaById(10L)).thenReturn(false);
        Exception exception = assertThrows(PersonaIdNotFoundException.class, () -> {
            personaRestController.getPersona(10L);
        });
    }

    @Test
    void createPersona() {
        when(personaService.existsPersonaByNumeroDocumento("85511568905")).thenReturn(false);
        PersonaCreationDTO personaCreationDTO = new PersonaCreationDTO("cc", "85511568905", "Andrés", "Pérez Rojas", 36, null, null);
        Persona persona = new Persona(0L ,
                personaCreationDTO.getTipoDocumento(),
                personaCreationDTO.getNumeroDocumento(),
                personaCreationDTO.getNombres(),
                personaCreationDTO.getApellidos(),
                personaCreationDTO.getEdad(),
                personaCreationDTO.getPaisOrigen(),
                personaCreationDTO.getSexo());
        when(personaModelMapper.personaCreationDtoToPersona(any(PersonaCreationDTO.class))).thenReturn(persona);
        when(personaService.savePersona(any(Persona.class))).thenReturn(persona);
        UriComponentsBuilder uriComponents = UriComponentsBuilder.newInstance()
                .scheme("http").host("127.0.0.1").port(8080).path("/api/personas");
        ResponseEntity<Void> result1 = personaRestController.createPersona(personaCreationDTO, uriComponents);
        assertEquals(HttpStatus.CREATED.value(), result1.getStatusCode().value());
        assertEquals("http://127.0.0.1:8080/api/personas/0",result1.getHeaders().get("location").get(0));

        when(personaService.existsPersonaByNumeroDocumento("85511568905")).thenReturn(true);
        assertThrows(NumeroDocumentoAlreadyExists.class, () -> {
            personaRestController.createPersona(personaCreationDTO, uriComponents);
        });
    }

    @Test
    void updatePersona() {
        when(personaService.existsPersonaById(0L)).thenReturn(true);
        when(personaService.findById(0L)).thenReturn(Optional.ofNullable(DatosPersona.PERSONA0));
        PersonaUpdateDTO personaUpdateDTO = new PersonaUpdateDTO("cc", "3792469932", "Daniel", "Porto Rojas", 28, "Perú", "M");
        Persona persona = new Persona(0L,
                personaUpdateDTO.getTipoDocumento(),
                personaUpdateDTO.getNumeroDocumento(),
                personaUpdateDTO.getNombres(),
                personaUpdateDTO.getApellidos(),
                personaUpdateDTO.getEdad(),
                personaUpdateDTO.getPaisOrigen(),
                personaUpdateDTO.getSexo()
            );
        when(personaModelMapper.personaUpdateDtoToPersona(any(PersonaUpdateDTO.class))).thenReturn(persona);
        when(personaModelMapper.personaToPersona(any(Persona.class), any(Persona.class))).thenReturn(persona);
        when(personaService.updatePersona(any(Persona.class))).thenReturn(persona);
        ResponseEntity<Persona> result1 = personaRestController.updatePersona(0L, personaUpdateDTO);
        assertEquals(HttpStatus.OK.value(), result1.getStatusCode().value());
        assertEquals(persona, result1.getBody());

        when(personaService.existsPersonaById(18L)).thenReturn(false);
        assertThrows(PersonaIdNotFoundException.class, () -> {
            personaRestController.updatePersona(18L, personaUpdateDTO);
        });
    }

    @Test
    void deletePersona() {
        when(personaService.existsPersonaById(0L)).thenReturn(true);
        ResponseEntity<Void> result1 = personaRestController.deletePersona(0L);
        assertEquals(HttpStatus.NO_CONTENT.value(), result1.getStatusCode().value());

        when(personaService.existsPersonaById(200L)).thenReturn(false);
        assertThrows(PersonaIdNotFoundException.class, () -> {
            personaRestController.deletePersona(200L);
        });
    }

    @Test
    void deleteAllPersonas() {
        ResponseEntity<Void> result1 = personaRestController.deleteAllPersonas();
        assertEquals(HttpStatus.NO_CONTENT.value(), result1.getStatusCode().value());
    }
}