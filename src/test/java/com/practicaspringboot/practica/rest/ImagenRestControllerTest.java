package com.practicaspringboot.practica.rest;

import com.practicaspringboot.practica.converters.ImagenModelMapper;
import com.practicaspringboot.practica.exceptions.ImagenAlreadyExistsForPersonId;
import com.practicaspringboot.practica.exceptions.ImagenNotFoundException;
import com.practicaspringboot.practica.exceptions.PersonaIdNotFoundException;
import com.practicaspringboot.practica.modelo.MongoDB.Imagen;
import com.practicaspringboot.practica.modelo.dto.imagen.ImagenCreationDTO;
import com.practicaspringboot.practica.modelo.dto.imagen.ImagenResponseDTO;
import com.practicaspringboot.practica.modelo.dto.imagen.ImagenUpdateDTO;
import com.practicaspringboot.practica.service.imagen.ImagenService;
import com.practicaspringboot.practica.service.persona.PersonaService;
import com.practicaspringboot.practica.util.DataTests.DatosImagen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ImagenRestControllerTest {

    @Mock
    private ImagenService imagenService;

    @Mock
    private PersonaService personaService;

    @Mock
    private ImagenModelMapper imagenModelMapper;

    @InjectMocks
    private ImagenRestController imagenRestController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void createImagen() throws IOException {
        when(personaService.existsPersonaById(0L)).thenReturn(true);
        when(imagenService.existsImagenByPersonaId(0L)).thenReturn(false);
        MultipartFile multipartFile = new MockMultipartFile("foo",
                "foo.txt", MediaType.IMAGE_JPEG.getType(),
                "I am an image".getBytes(StandardCharsets.UTF_8));
        ImagenCreationDTO imagenCreationDTO = new ImagenCreationDTO(multipartFile);
        Imagen imagen = new Imagen("ff14fgefglpñrdzs", 0L, multipartFile.getBytes());
        ImagenResponseDTO imagenResponseDTO = new ImagenResponseDTO(imagen.getId(), imagen.getPersonaId(), imagen.getFile());
        when(imagenModelMapper.ImagenCreationDtoToImagen(any(ImagenCreationDTO.class))).thenReturn(imagen);
        when(imagenModelMapper.ImagenToImagen(any(Imagen.class), any(Imagen.class))).thenReturn(imagen);
        when(imagenModelMapper.imagenToImagenResponseDto(any(Imagen.class))).thenReturn(imagenResponseDTO);
        when(imagenService.saveImagen(any(Imagen.class))).thenReturn(imagen);

        ResponseEntity<ImagenResponseDTO> result1 = imagenRestController.createImagen(0L, multipartFile, imagenCreationDTO);
        assertEquals(HttpStatus.CREATED.value(), result1.getStatusCode().value());
        assertEquals("ff14fgefglpñrdzs", result1.getBody().getId());

        when(personaService.existsPersonaById(19L)).thenReturn(false);
        assertThrows(PersonaIdNotFoundException.class, () -> {
            imagenRestController.createImagen(19L, multipartFile, imagenCreationDTO);
        });

        when(personaService.existsPersonaById(14L)).thenReturn(true);
        when(imagenService.existsImagenByPersonaId(14L)).thenReturn(true);
        assertThrows(ImagenAlreadyExistsForPersonId.class, () -> {
            imagenRestController.createImagen(14L, multipartFile, imagenCreationDTO);
        });
    }

    @Test
    void deleteImagen() {
        when(imagenService.existsImagenByPersonaId(1L)).thenReturn(true);
        when(imagenService.findImagenByPersonaId(1L)).thenReturn(Optional.ofNullable(DatosImagen.IMAGEN1));
        ResponseEntity<Void> result1 = imagenRestController.deleteImagen(1L);
        assertEquals(HttpStatus.NO_CONTENT.value(), result1.getStatusCode().value());

        when(imagenService.existsImagenByPersonaId(8L)).thenReturn(false);
        assertThrows(ImagenNotFoundException.class, () -> {
            imagenRestController.deleteImagen(8L);
        });
    }

    @Test
    void getImagen() {
        when(imagenService.findImagenByPersonaId(0L)).thenReturn(Optional.ofNullable(DatosImagen.IMAGEN0));
        when(imagenService.existsImagenByPersonaId(0L)).thenReturn(true);
        ImagenResponseDTO imagenResponseDTO = new ImagenResponseDTO(
                DatosImagen.IMAGEN0.getId(),
                DatosImagen.IMAGEN0.getPersonaId(),
                DatosImagen.IMAGEN0.getFile()
        );
        when(imagenModelMapper.imagenToImagenResponseDto(any(Imagen.class))).thenReturn(imagenResponseDTO);
        ResponseEntity<ImagenResponseDTO> result1 = imagenRestController.getImagen(0L);
        assertEquals(HttpStatus.OK.value(), result1.getStatusCode().value());
        assertEquals("47783578834", result1.getBody().getId());

        when(imagenService.existsImagenByPersonaId(20L)).thenReturn(false);
        assertThrows(ImagenNotFoundException.class, () -> {
            imagenRestController.getImagen(20L);
        });
    }

    @Test
    void updatePersona() throws IOException {
        when(imagenService.existsImagenByPersonaId(1L)).thenReturn(true);
        when(imagenService.findImagenByPersonaId(1L)).thenReturn(Optional.ofNullable(DatosImagen.IMAGEN1));
        MultipartFile multipartFile = new MockMultipartFile("foo",
                "foo.txt", MediaType.IMAGE_JPEG.getType(),
                "I am an image".getBytes(StandardCharsets.UTF_8));
        ImagenUpdateDTO imagenUpdateDTO = new ImagenUpdateDTO(multipartFile);
        Imagen imagen = new Imagen(DatosImagen.IMAGEN1.getId(), 1L, multipartFile.getBytes());
        imagen.setFile(imagenUpdateDTO.getFile().getBytes());
        when(imagenModelMapper.ImagenUpdateDtoToImagen(any(ImagenUpdateDTO.class))).thenReturn(imagen);
        when(imagenModelMapper.ImagenToImagen((any(Imagen.class)), any(Imagen.class))).thenReturn(imagen);
        when(imagenService.updateImagen(any(Imagen.class))).thenReturn(imagen);
        ImagenResponseDTO imagenResponseDTO = new ImagenResponseDTO(imagen.getId(), imagen.getPersonaId(), imagen.getFile());
        when(imagenModelMapper.imagenToImagenResponseDto(any(Imagen.class))).thenReturn(imagenResponseDTO);


        ResponseEntity<ImagenResponseDTO> result1 = imagenRestController.updateImagen(1L, multipartFile, imagenUpdateDTO);
        assertEquals(HttpStatus.OK.value(), result1.getStatusCode().value());
        assertEquals("d873dudbbd3", result1.getBody().getId());

        when(imagenService.existsImagenByPersonaId(45L)).thenReturn(false);
        assertThrows(ImagenNotFoundException.class, () -> {
            imagenRestController.updateImagen(45L, multipartFile, imagenUpdateDTO);
        });
    }
}