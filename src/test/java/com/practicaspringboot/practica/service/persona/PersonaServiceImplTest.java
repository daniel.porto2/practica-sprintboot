package com.practicaspringboot.practica.service.persona;

import com.practicaspringboot.practica.modelo.MySQL.Persona;
import com.practicaspringboot.practica.repository.MySQL.PersonaRepository;
import com.practicaspringboot.practica.util.DataTests.DatosPersona;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
public class PersonaServiceImplTest {

    @Mock
    private PersonaRepository personaRepository;

    @InjectMocks
    private PersonaServiceImpl personaService;

    @BeforeEach
    void SetUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void findAllPersonas() {
        when(personaRepository.findAll()).thenReturn(DatosPersona.PERSONALIST);
        List<Persona> personas = personaService.findAllPersonas();
        assertNotNull(personas);
        assertEquals(2, personas.size());
    }

    @Test
    void findById() {
        when(personaRepository.findById(0L)).thenReturn(Optional.of(DatosPersona.PERSONA0));
        Optional<Persona> persona = personaService.findById(0L);
        assertNotNull(persona);
        assertEquals("Daniel", persona.get().getNombres(), "El nombre de la persona obtenida no coincide");
        assertEquals("3792469932", persona.get().getNumeroDocumento(),"El numeroDocumento de la persona obtenida no coincide");

    }

    @Test
    void findByNumeroDocumento() {
        when(personaRepository.findByNumeroDocumento("844633804")).thenReturn(DatosPersona.PERSONA1);
        Persona persona = personaService.findByNumeroDocumento("844633804");
        assertNotNull(persona);
        assertEquals("Hernandez Oviedo", persona.getApellidos());
    }

    @Test
    void savePersona() {
        saveMockIncrementalId();

        Persona newPersona = DatosPersona.NEW_PERSONA;

        Persona savedPerson = personaService.savePersona(newPersona);
        assertNotNull(savedPerson);
        assertEquals("12848908968", savedPerson.getNumeroDocumento());
        assertEquals(5L, savedPerson.getId());
        verify(personaRepository, times(1)).save(newPersona);
    }

    @Test
    void updatePersona() {
        saveMock();
        Persona updatedPersona = personaService.updatePersona(DatosPersona.PERSONA0);
        assertEquals("3792469932", updatedPersona.getNumeroDocumento());
        assertEquals(0L, updatedPersona.getId());
    }

    @Test
    void existsPersonaById() {
        when(personaRepository.existsById(0L)).thenReturn(true);
        when(personaRepository.existsById(4L)).thenReturn(false);
        assertEquals(true, personaService.existsPersonaById(0L));
        assertEquals(false, personaService.existsPersonaById(4L));
    }

    @Test
    void existsPersonaByNumeroDocumento() {
        when(personaRepository.existsByNumeroDocumento("3792469932")).thenReturn(true);
        when(personaRepository.existsByNumeroDocumento("093884")).thenReturn(false);
        assertEquals(true, personaService.existsPersonaByNumeroDocumento("3792469932"));
        assertEquals(false, personaService.existsPersonaByNumeroDocumento("093884"));
    }

    @Test
    void deletePersonaById(){
        personaService.deletePersonaById(4L);
        verify(personaRepository, times(1)).deleteById(4L);
    }

    void saveMockIncrementalId() {
        when(personaRepository.save(any(Persona.class))).then(new Answer<Persona>() {
            Long sequence = 5L;
            @Override
            public Persona answer(InvocationOnMock invocation) throws Throwable{
                Persona persona = invocation.getArgument(0);
                persona.setId(sequence++);
                return persona;
            }
        });
    }

    void saveMock() {
        when(personaRepository.save(any(Persona.class))).then(new Answer<Persona>() {
            @Override
            public Persona answer(InvocationOnMock invocation) throws Throwable{
                Persona persona = invocation.getArgument(0);
                return persona;
            }
        });
    }
}
