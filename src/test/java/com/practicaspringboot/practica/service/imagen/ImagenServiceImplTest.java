package com.practicaspringboot.practica.service.imagen;

import com.practicaspringboot.practica.modelo.MongoDB.Imagen;
import com.practicaspringboot.practica.repository.mongoDB.ImagenRepository;
import com.practicaspringboot.practica.util.DataTests.DatosImagen;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class ImagenServiceImplTest {

    @Mock
    private ImagenRepository imagenRepository;

    @InjectMocks
    private ImagenServiceImpl imagenService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void findById() {
        when(imagenRepository.findById("47783578834")).thenReturn(Optional.of(DatosImagen.IMAGEN0));
        Optional<Imagen> imagen = imagenService.findById("47783578834");
        assertNotNull(imagen);
        assertEquals(0L, imagen.get().getPersonaId());
    }

    @Test
    void saveImagen() {
        saveMockIncrementalId();
        Imagen imagen = imagenService.saveImagen(DatosImagen.IMAGEN0);
        assertNotNull(imagen);
        assertEquals(0L, imagen.getPersonaId());
    }

    @Test
    void updateImagen() {
        saveMock();
        Imagen imagen = imagenService.updateImagen(DatosImagen.IMAGEN0);
        assertNotNull(imagen);
        assertEquals(0L, imagen.getPersonaId());
    }

    @Test
    void findImagenByPersonaId() {
        when(imagenRepository.findByPersonaId(1L)).thenReturn(Optional.of(DatosImagen.IMAGEN1));
        Optional<Imagen> imagen = imagenService.findImagenByPersonaId(1L);
        assertNotNull(imagen);
        assertEquals("d873dudbbd3", imagen.get().getId());
    }

    @Test
    void existsImagenById() {
        when(imagenRepository.existsById("d873dudbbd3")).thenReturn(true);
        assertTrue(imagenService.existsImagenById("d873dudbbd3"));
    }

    @Test
    void existsImagenByPersonaId() {
        when(imagenRepository.existsByPersonaId(8L)).thenReturn(false);
        assertFalse(imagenService.existsImagenByPersonaId(8L));
    }

    void saveMockIncrementalId() {
        when(imagenRepository.save(any(Imagen.class))).then(new Answer<Imagen>() {
            int sequence = 0;
            @Override
            public Imagen answer(InvocationOnMock invocation) throws Throwable{
                Imagen imagen = invocation.getArgument(0);
                sequence++;
                imagen.setId(Integer.toString(0));
                return imagen;
            }
        });
    }

    void saveMock() {
        when(imagenRepository.save(any(Imagen.class))).then(new Answer<Imagen>() {
            @Override
            public Imagen answer(InvocationOnMock invocation) throws Throwable{
                Imagen imagen = invocation.getArgument(0);
                return imagen;
            }
        });
    }
}